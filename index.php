<?php $current = 1 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- on inclut le fhead -->
    <?php include "includes/head.php" ?>
    <title>Immo</title>
</head>

<body>
    <!-- on inclut la navbar -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">
        <!--jumbotron-->
        <div class="jumbotron jumbotron-fluid">
            <h2 class="agence">Agence immobilière</h2>
        </div>

        <div class="row">
            <aside class="col-md-4">
                <h4>Bienvenue sur le site de l'Agence Immobilière Morbihannaise !</h4>
            </aside>

            <section class="col-md-8">
                <p>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Esse ratione, iusto in harum aperiam, consectetur aut dolorum est quis possimus placeat, recusandae alias hic asperiores rem? Optio inventore eligendi perferendis?
                </p>
                <p>
                    Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
                </p>
            </section>

        </div>

        <!--saut de ligne-->
        <br>

        <!-- on inclut le footer -->
        <?php include "includes/footer.php" ?>

    </div>

</body>

</html>