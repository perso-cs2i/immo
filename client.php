<?php $current = 7 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Liste des clients</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">
        
        <?php
        // Créer une instruction SQL
        $sql = "SELECT * 
            FROM villes INNER JOIN clients ON villes.codeville = clients.codeville 
            ORDER BY nomclient,prenomclient";
        $sql2 = "SELECT COUNT(*) as total FROM clients";

        // Créer et éxécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        $requete2 = $pdo->prepare($sql2);
        $requete2->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listePersonnes = $requete->fetchAll();
        $compte = $requete2->fetchAll();

        // On peut maintenant afficher les données
        ?>
        <h2>
            Liste des clients (<?php foreach ($compte as $c)
        {
                echo $c['total'];
        } ?>)
        </h2>
        
        <!-- Tableau qui affiche les infos -->
        <!--<style type=text/css> table tbody tr:nth-child(odd) { background-color: #d7d7d7; } </style> -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Code postal</th>
                    <th scope="col">Ville</th>
                </tr>
            </thead>
            <tbody>
            
                <?php foreach ($listePersonnes as $client) { ?>
                    <tr>
                        <td><?php echo $client['nomclient'] ?></td>
                        <td><?php echo $client['prenomclient'] ?></td>
                        <td><?php echo $client['titre'] ?></td>
                        <td><?php echo $client['codepostal'] ?></td>
                        <td><?php echo $client['nomville'] ?></td>
                    </tr>
                    <?php } ?>

            </tbody>
           
        </table>


    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>