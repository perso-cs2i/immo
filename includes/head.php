<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--Chargement CSS de Bootstrap-->
<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="lib/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

<?php
    //inclure le fichier de connexion ()ouvrir la connexion)
    include "data/connection.php";