<!--header-->
<div class="header">
    <!--navbar boostrap-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!--navigation pure -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!--partie gauche (définie par la première balise englobante)-->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Immo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="proprietaire.php">Propriétaires</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="biens.php">Biens</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="client.php">Clients</a>
                </li>
            </ul>
            <!--2eme balise englobante définit la partie droite du menu-->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="qui.php">Qui sommes-nous ?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="plan.php">Plan d'accès</a>
                </li>
            </ul>
        </div>
    </nav>
</div>