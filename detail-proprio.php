<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Détail Propriétaire</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">

        <?php $id = $_GET['id'];
        // Créer une instruction SQL
        $sql = "SELECT * FROM proprietaires WHERE proprietaires.numeroproprietaire = $id";

        $sql2 = "SELECT * FROM biens    
        INNER JOIN villes ON biens.codeville = villes.codeville
        INNER JOIN typestransactions ON biens.codetransaction = typestransactions.codetransaction
        INNER JOIN typesbiens ON biens.codebien = typesbiens.codebien
        WHERE biens.numeroproprietaire = $id
        ORDER BY montant DESC";

        $sql3 = "SELECT COUNT(biens.numeroproprietaire) as total 
        FROM proprietaires 
        INNER JOIN biens ON proprietaires.numeroproprietaire = biens.numeroproprietaire
        WHERE proprietaires.numeroproprietaire = $id";

        // Créer et éxécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        $requete2 = $pdo->prepare($sql2);
        $requete2->execute();

        $requete3 = $pdo->prepare($sql3);
        $requete3->execute();


        // Récupérer les lignes de tables qui correspondent à la requête
        $details = $requete->fetchAll();
        $listeBiens = $requete2->fetchAll();
        $compte = $requete3->fetchAll();


        //recupérer dans la variable $id l'id transmis dans l'url

        ?>
        <div class="row">
            <div class="col-md-4">
                <h1>Propriétaire</h1>
                <h2> <?php foreach ($details as $detail) {
                            echo $detail['nomproprietaire'] . ' ' . $detail['prenomproprietaire']; ?>
                </h2>
            <?php
                        }
            ?>

            <table class="table">
                <tr>
                    <td>Nom</td>
                    <td><?php echo $detail['nomproprietaire'] ?></td>
                </tr>

                <tr>
                    <td>Prénom</td>
                    <td><?php echo $detail['prenomproprietaire'] ?></td>
                </tr>

                <tr>
                    <td>Téléphone personnel</td>
                    <td><?php echo $detail['telephonepersonnel'] ?></td>
                </tr>

                <tr>
                    <td>Téléphone mobile</td>
                    <td><?php echo $detail['telephonemobile'] ?></td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td><?php echo $detail['email'] ?></td>
                </tr>
            </table>




            </div>

            <div class="col-md-7">
                <!-- Tableau qui affiche les infos -->
                <!--<style type=text/css> table tbody tr:nth-child(odd) { background-color: #d7d7d7; } </style> -->
                <h2>
                    Liste des biens (<?php foreach ($compte as $c) {
                                            echo $c['total'];
                                        } ?>)
                </h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Adresse</th>
                            <th scope="col">Code postal</th>
                            <th scope="col">Villes</th>
                            <th scope="col">Transaction</th>
                            <th scope="col">Type de bien</th>
                            <th scope="col">Pièces</th>
                            <th scope="col">Montant</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($listeBiens as $bien) { ?>
                            <tr>

                                <td><?php echo $bien['adresse1'] ?></td>
                                <td><?php echo $bien['codepostal'] ?></td>
                                <td><?php echo $bien['nomville'] ?></td>
                                <td><?php echo $bien['intituletransaction'] ?></td>
                                <td><?php echo $bien['intitulebien'] ?></td>
                                <td>
                                    <?php
                                    if ($bien['pieces'] >= 3) { ?>
                                        <span class="badge badge-success"><?php echo $bien['pieces']; ?></span>
                                    <?php
                                    } else { ?>
                                        <span class="badge badge-dark"><?php echo $bien['pieces']; ?></span>
                                    <?php
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    if ($bien['montant'] > 300000) { ?>
                                        <span class="montant"><?php echo $bien['montant']; ?></span>
                                    <?php } else {
                                        echo $bien['montant'];
                                    }
                                    ?>

                                </td>
                            </tr>
                        <?php } ?>

                    </tbody>

                </table>
            </div>
        </div>

    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>