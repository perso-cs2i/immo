<?php $current = 6 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Contact</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>
 
    <!--contenu-->
    <div class="container">
        <!--jumbotron-->
        <div class="jumbotron jumbotron-fluid">
            <h2 class="constru">Page en construction</h2>
            <p class="pconstru">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Asperiores ratione molestias ut accusantium rerum dolor fuga eveniet deserunt, corporis placeat labore, dolore suscipit libero temporibus dolorem nihil tenetur beatae blanditiis.</p>
        </div>
    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>