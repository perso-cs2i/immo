<?php
/*
Mise en place de la connexion PDO -Version simplifiée
*/

// Inclure le fichier de configuration
require 'config.php';

// Créer un DSN (Data Source Name) ou "chaine de connexion"
// driver:host=SERVEUR;dbname=BASE;port=PORT;"

// Pour MySQL :
/* ancienne version 
$dsn = "mysql:host=localhost;dbname=demo;port=3306;"; */
$dsn = sprintf("%s:host=%s;dbname=%s;port=%d;charset=UTF8;",
    $dbconfig['driver'],
    $dbconfig['host'],
    $dbconfig['database'],
    $dbconfig['port']
    
);

// Créer un objet PDO => ouvrir la connexion
$pdo = new PDO($dsn,
    $dbconfig['username'],
    $dbconfig['password']
);

