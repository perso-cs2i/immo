<?php

// Exemple de configuration pour MySQL sur Wamp
$dbconfig = 
[
    'host' => 'localhost',
    'port' => 3306,
    'database' => 'YOURDATABASE',
    'username' => 'root',
    'password' => ''
];
