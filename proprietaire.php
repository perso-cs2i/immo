<?php $current = 7 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Liste des proprietaires</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">

        <?php
        // Créer une instruction SQL
        $sql = "SELECT * FROM proprietaires ORDER BY nomproprietaire,prenomproprietaire";
        $sql2 = "SELECT COUNT(*) as total FROM proprietaires";


        // Créer et éxécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        $requete2 = $pdo->prepare($sql2);
        $requete2->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listePersonnes = $requete->fetchAll();
        $compte = $requete2->fetchAll();

        // On peut maintenant afficher les données
        ?>
        <h2>
            Liste des proprietaires (<?php foreach ($compte as $c) {
                                            echo $c['total'];
                                        } ?>)
        </h2>

        <!-- Tableau qui affiche les infos -->
        <!--<style type=text/css> table tbody tr:nth-child(odd) { background-color: #d7d7d7; } </style> -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Lien</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Téléphone perso.</th>
                </tr>
            </thead>
            <tbody>
            
                <?php foreach ($listePersonnes as $proprio) { ?>
                    <tr>
                        <td>
                            <?php 
                            $idP = $proprio['numeroproprietaire'];
                            
                            ?>
                        <a href="detail-proprio.php?id=<?php echo $idP ?>"> <?php echo $proprio['nomproprietaire'] ?> </a>

                        </td>
                        
                        <td><?php echo $proprio['nomproprietaire'] ?></td>
                        <td><?php echo $proprio['prenomproprietaire'] ?></td>
                        <td><?php echo $proprio['titre'] ?></td>
                        <td><?php echo $proprio['telephonemobile'] ?></td>
                        <td><?php echo $proprio['telephonepersonnel'] ?></td>
                    </tr>
                    <?php } ?>

            </tbody>
           
        </table>


    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>