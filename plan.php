<?php $current = 7 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Plan</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">
        <h1>Plan d'accès</h1>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2682.3269001449908!2d-3.369590487445986!3d47.755713266662035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf7c8d67ac1f7deb8!2sCCI%20Formation%20Lorient!5e0!3m2!1sfr!2sfr!4v1586331973468!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>