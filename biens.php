<?php $current = 7 ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- En-tête technique de la page -->
    <?php include "includes/head.php" ?>
    <!--
Le titre ne fait pas partie de head.php parce qu'il
doit être différent pour chaque page
-->
    <title>Liste des biens</title>
</head>

<body>
    <!-- Menu (Navbar Bootstrap) -->
    <?php include "includes/navigation.php" ?>

    <!--saut de ligne-->
    <br>

    <!--contenu-->
    <div class="container">
        
        <?php
        // Créer une instruction SQL
        $sql = "SELECT * FROM biens 
        INNER JOIN villes ON biens.codeville = villes.codeville
        INNER JOIN typestransactions ON biens.codetransaction = typestransactions.codetransaction
        INNER JOIN typesbiens ON biens.codebien = typesbiens.codebien
        ORDER BY montant DESC";
            
        $sql2 = "SELECT COUNT(*) as total FROM biens";

        // Créer et éxécuter une requête PDO
        $requete = $pdo->prepare($sql);
        $requete->execute();

        $requete2 = $pdo->prepare($sql2);
        $requete2->execute();

        // Récupérer les lignes de tables qui correspondent à la requête
        $listeBiens = $requete->fetchAll();
        $compte = $requete2->fetchAll();

        // On peut maintenant afficher les données
        ?>
        <h2>
            Liste des biens (<?php foreach ($compte as $c)
        {
                echo $c['total'];
        } ?>)
        </h2>

        <!-- Tableau qui affiche les infos -->
        <!--<style type=text/css> table tbody tr:nth-child(odd) { background-color: #d7d7d7; } </style> -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Adresse</th>
                    <th scope="col">Code postal</th>
                    <th scope="col">Villes</th>
                    <th scope="col">Transaction</th>
                    <th scope="col">Type de bien</th>
                    <th scope="col">Pièces</th>
                    <th scope="col">Montant</th>
                </tr>
            </thead>
            <tbody>
            
                <?php foreach ($listeBiens as $bien) { ?>
                    <tr>
             
                        <td><?php echo $bien['adresse1'] ?></td>
                        <td><?php echo $bien['codepostal'] ?></td>
                        <td><?php echo $bien['nomville'] ?></td>
                        <td><?php echo $bien['intituletransaction'] ?></td>
                        <td><?php echo $bien['intitulebien'] ?></td>
                        <td>
                            <?php
                                if ($bien['pieces'] >= 3)
                                { ?>
                                    <span class="badge badge-success"><?php echo $bien['pieces'];?></span>
                                <?php
                                }
                                else 
                                { ?>
                                    <span class="badge badge-dark"><?php echo $bien['pieces'];?></span>
                                <?php
                                }
                                ?>
                        </td>
                        
                        <td>
                            <?php 
                                if ($bien['montant'] > 300000)
                                { ?>
                                    <span class="montant"><?php echo $bien['montant'];?></span>
                                <?php }
                                else
                                {
                                    echo $bien['montant'];
                                }
                            ?>
                            
                        </td>
                    </tr>
                    <?php } ?>

            </tbody>
           
        </table>


    </div>
    <!-- Pied de page -->
    <?php include "includes/footer.php" ?>
</body>

</html>